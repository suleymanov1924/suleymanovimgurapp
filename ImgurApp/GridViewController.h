//
//  GridViewController.h
//  ImgurApp
//
//  Created by Артем Сулейманов on 27.11.2017.
//  Copyright © 2017 Артем Сулейманов. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReloadViewController.h"
#import "ImgurServiceLayer.h"
#import "CollectionViewCell.h"
#import "ImgurAppModel.h"
#import "DetailViewController.h"

@interface GridViewController : ReloadViewController <UICollectionViewDelegate, UICollectionViewDataSource>


@property(nonatomic,strong) ImgurServiceLayer * serviceObject;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

-(void)reloadInterface;

@end
