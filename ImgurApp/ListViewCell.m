//
//  ListViewCell.m
//  ImgurApp
//
//  Created by Артем Сулейманов on 27.11.2017.
//  Copyright © 2017 Артем Сулейманов. All rights reserved.
//

#import "ListViewCell.h"

@implementation ListViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Инициализируем код
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Настраиваем представление
}

@end
