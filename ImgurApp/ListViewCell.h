//
//  ListViewCell.h
//  ImgurApp
//
//  Created by Артем Сулейманов on 27.11.2017.
//  Copyright © 2017 Артем Сулейманов. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListViewCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UIImageView *imageViewInCell;



@end
