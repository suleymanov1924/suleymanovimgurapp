//
//  ImgurAppModel.h
//  ImgurApp
//
//  Created by Артем Сулейманов on 27.11.2017.
//  Copyright © 2017 Артем Сулейманов. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ImgurAppModel : NSObject



@property(nonatomic) NSString * image;
@property(nonatomic) NSString * title;
@property(nonatomic) NSString * imgurDescription;
@property(nonatomic) NSString * upvotes;
@property(nonatomic) NSString * downvotes;
@property(nonatomic) NSString * score;




@end
