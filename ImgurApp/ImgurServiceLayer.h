//
//  ImgurServiceLayer.h
//  ImgurApp
//
//  Created by Артем Сулейманов on 27.11.2017.
//  Copyright © 2017 Артем Сулейманов. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ReloadViewController.h"
#import <UIKit/UIKit.h>
#import "ImgurAppModel.h"



@interface ImgurServiceLayer : NSObject


@property(nonatomic, strong)NSURLSession * session;
@property(nonatomic, strong)NSDictionary * jsonDictionary;
@property(nonatomic, strong)NSArray * jsonArray;

@property(nonatomic, strong)NSMutableArray * arrayWithImgurAppModelObjects;



-(void) doApiRequest:(UIViewController*)vc;


@end
