//
//  ViewController.h
//  ImgurApp
//
//  Created by Артем Сулейманов on 27.11.2017.
//  Copyright © 2017 Артем Сулейманов. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReloadViewController.h"
#import "ImgurServiceLayer.h"
#import "ListViewCell.h"
#import "ImgurAppModel.h"
#import "DetailViewController.h"


@interface ViewController : ReloadViewController <UITableViewDelegate, UITableViewDataSource>

@property(nonatomic,strong) ImgurServiceLayer * serviceObject;

@property(nonatomic, strong) NSString * title;
@property (weak, nonatomic) IBOutlet UITableView *tableView;


-(void)reloadInterface;

@end

