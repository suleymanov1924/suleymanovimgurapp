//
//  CollectionViewCell.h
//  ImgurApp
//
//  Created by Артем Сулейманов on 27.11.2017.
//  Copyright © 2017 Артем Сулейманов. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionViewCell : UICollectionViewCell



@property (weak, nonatomic) IBOutlet UIImageView *imageView;


@end
